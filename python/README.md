# Python implementation of GOBNILP

GOBNILP is a program that uses Gurobi to
learn Bayesian network structure from complete discrete data (or
precomputed local scores).

For full details on GOBNILP (including the C version), please consult the GOBNILP page:

https://www.cs.york.ac.uk/aig/sw/gobnilp/

# Running GOBNILP
For more details run:
`python simple_gobnilp`

# Scoring code

## Running the code
To use this code it is assumed that you have Anaconda Python installed. 
If you want to use the scoring code you will also need to install Numba (this be done easily using Conda). 

Tested using Numba 0.4.3 and Python 3.7.1

To use the scoring code run:

`python scoring.py <data file> <outputfile name>`

To see all the available options please run:
`python scoring.py`

## Test data files
The test_data_and_scores folder contains all the datasets (*.dat) and pre-computed scores (*.jkl) by the C version that are used by the tests.

## Running the tests
Various tests have been written for this code.
The tests are located in the tests folder to run a single test go to the root folder of this repository and run, for example:

`python -m unittest tests.test_scoring_pandas_end_to_end`

To run all of them run:
`python -m unittest discover tests`

## Reusing the ADTree/contingency table generator
Both the ADTree and contingency table generator require that when making a contingency table the attributes used are stored in numpy array in ascending order and there is at least one attribute. They cannot create contingency tables for 0 attributes...