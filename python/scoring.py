#!/usr/bin/env python3


__docformat__ = 'restructuredtext'

from math import lgamma

import numpy as np

from itertools import combinations

import sys

from numba import jit

import argparse
    
@jit(nopython=True)
def _process_tree_contab_inner(arities, contab_tree, tree_index, att_index, variables, alpha_div_arities):
    """
        Recursively processes a contingency table tree that is stored in an array.
        
        Parameters:
        
        - `arities`: contains the arities of every variable
        - `contab_tree`: the tree contingency table
        - `tree_index`: The current array index that is being examined
        - `att_index`: The index of the current attribute in variables
        - `variables`: The variables for which the contingency table was constructed
        - `alpha_div_arities`: Alpha divided by the product of the arities of the variables
    """
    score = 0.0
    non_zero_count = 0
    for val in range(arities[variables[att_index]]):
        next_tree_index = tree_index + val

        if contab_tree[next_tree_index] != 0:
            if att_index == variables.size - 1:
                # count value
                score -= lgamma(alpha_div_arities+contab_tree[next_tree_index])
                non_zero_count += 1
            else:
                add_to_score, add_to_non_zero_count = _process_tree_contab_inner(arities, contab_tree, contab_tree[next_tree_index], att_index+1, variables, alpha_div_arities)
                score += add_to_score
                non_zero_count += add_to_non_zero_count
    return score, non_zero_count
    
@jit(nopython=True)
def _process_tree_contab(arities, contab, variables, alpha_div_arities):
    """
        Recursively processes a contingency table tree that is stored in an array.
        Calculates the BDeu "score component" for that table.
        
        Parameters:
        
        - `arities`: contains the arities of every variable
        - `contab`: the tree contingency table
        - `variables`: The variables for which the contingency table was constructed
        - `alpha_div_arities`: Alpha divided by the product of the arities of the variables
    """
    score, non_zero_count = _process_tree_contab_inner(arities,contab, 0, 0, variables, alpha_div_arities)
    score += non_zero_count*lgamma(alpha_div_arities)  
    return score

# This must have the same signature as _process_tree_contab
@jit(nopython=True)    
def _process_array_contab(arities, contab, variables, alpha_div_arities):
    non_zero_count = 0
    score = 0
    for count in contab:
        if count != 0:
            non_zero_count+=1
            score -= lgamma(alpha_div_arities+count) 
    
    score += non_zero_count*lgamma(alpha_div_arities)  
    return score

# This was moved out of the class as it can easily be accelerated with Numba
@jit(nopython=True)
def _bdeu_score_component(contab_generator, arities, variables, alpha, process_contab_function):
    """
        Parameters:

        - `contab_generator`: used to generate the necessary contingency tables
        - `arities`: A NumPy array containing the arity of every variable
        - `variables`: The variables for which the score component is being constructed
        - `alpha`: Effective sample size
    """
    alpha_div_arities = alpha / arities[variables].prod()
    
    contab = contab_generator.make_contab(variables)
    score = process_contab_function(arities, contab, variables, alpha_div_arities)             
    
    return score

def save_local_scores(local_scores, filename):
    variables = local_scores.keys()
    with open(filename, "w") as scores_file:
        scores_file.write(str(len(variables)))
        for child, dkt in local_scores.items():
            scores_file.write("\n" + child + " " + str(len(dkt.keys())))
            for parents, score in dkt.items():
                #highest_sup = None
                scores_file.write("\n" + str(score) + " " + str(len(parents)) +" "+ " ".join(parents))

class BDeuScoresGenerator:
    """
    Used to calcuate BDeuScores for a dataset of complete discrete data
    """
    
    def __init__(self,fname, use_adtree = False, rmin=32, max_palim_size=None):
        """"
        Parameters:

        - `fname`: Filename containing the data. It is assumed that:
            1. All values are separated by whitespace
            2. Comment lines start with a '#'
            3. The first line is a header line stating the names of the variables
            4. The second line states the arities of the variables
            5. All other lines contain the actual data
        - `use_adtree`: if True an ADTree will be used to compute the necessary
            counts. Otherwise a simpiler approach is taken (it is recommended 
            that you only use an ADTree if you have a large number of records
            in your dataset).
            
        Optional parameters:
        - `rmin`: the rmin value for the ADTree
            (Only use when using an ADTree)
        - `max_palim_size`: The maximum size of any parent set. 
            Setting this can lead to faster computation.
            (Only use when using an ADTree)
        """
        
        with open(fname, "r") as file:
            variables = file.readline().split()
            arities = np.array([int(x) for x in file.readline().split()],dtype=np.uint32)
            data = np.loadtxt(file,
                                 dtype=np.uint32,
                                 comments='#')
        self._data_length = data.shape[0]    
        self._arities = arities
        self._variables = variables
        self._varidx = {}
        for i, v in enumerate(self._variables):
            self._varidx[v] = i
        
        # When Numba compiled classes are imported
        # they are compiled regardless of whether or not they are used.
        # Therefore we only want to import the class that we will actually be
        # using. (Note: it appears that importing a class multiple times does not 
        # lead to it being compiled multiple times.)
        if use_adtree:
            from ADTree import ADTree
            self.process_contab_function = _process_array_contab
            if max_palim_size == None:
                max_palim_size = arities.size - 1
            self._contab_generator = ADTree(data, arities, rmin, max_palim_size+1)
        else:
            from contab_simple_tree import ContabGenerator
            self.process_contab_function = _process_tree_contab
            self._contab_generator = ContabGenerator(data, arities)
        
    
    def all_bdeu_scores(self,alpha=1.0,palim=None):
        """
        Exhaustively compute all BDeu scores and upper bounds for all families up to `palim`
        Return a dictionary dkt where dkt[child][parents] = bdeu_score
        """
        
        if palim == None:
            palim = self._arities.size - 1

        score_dict = {}
        # Initialisation
        # Need to create dict for every child
        # also its better to do the zero size parent set calc here
        # so that we don't have to do a check for every parent set
        # to make sure it is not of size 0 when calculating score component size
        no_parents_score_component = lgamma(alpha) - lgamma(alpha + self._data_length)
        for c, child in enumerate(self._variables):
            score_dict[child] = {
                frozenset([]):
                no_parents_score_component
            }
        
        for pasize in range(1,palim+1):
            for parents in combinations(self._variables,pasize): 
                
                variables = np.array([self._varidx[x] for x in list(parents)], dtype=np.uint32)
                score_component = _bdeu_score_component(self._contab_generator, self._arities, variables, alpha, self.process_contab_function)
                parents_set = frozenset(parents)
                for child in self._variables:
                    if child in parents_set:
                        score_dict[child][parents_set.difference([child])] -= score_component
                    else:
                        score_dict[child][parents_set] = score_component 
                
                    
        for vars in combinations(self._variables,palim+1):
            variables = np.array([self._varidx[x] for x in list(vars)], dtype=np.uint32)
            vars_set = frozenset(vars)
            score_component = _bdeu_score_component(self._contab_generator, self._arities, variables, alpha, self.process_contab_function)
            for child in vars:
                score_dict[child][vars_set.difference([child])] -= score_component
            
        return score_dict

if __name__ == '__main__':
    parser = argparse.ArgumentParser(description='BDeu local score generation (for Bayesian network learning)')
    parser.add_argument('data_file', help="Data file in GOBNILP format")
    parser.add_argument('scores_file', help="Where to store the generated scores")
    parser.add_argument('-p','--palim', type=int, help="Parent set size limit")
    parser.add_argument('-a', '--alpha', type=float, default=1.0, help="The effective sample size")
    parser.add_argument('-t', '--adtree', action="store_true", help="Use and ADTree")
    parser.add_argument('-r', '--rmin', type=int, default=32, help = "Rmin value for the ADTree")

    args = parser.parse_args()
    
    local_scores_generator = BDeuScoresGenerator(args.data_file, use_adtree = args.adtree, rmin=args.rmin, max_palim_size=args.palim)
    local_scores = local_scores_generator.all_bdeu_scores(args.alpha,args.palim)
    save_local_scores(local_scores, args.scores_file)
    