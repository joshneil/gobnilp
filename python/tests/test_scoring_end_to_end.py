import unittest

import tempfile
import os

from scoring import BDeuScoresGenerator, save_local_scores
from glob import glob
from itertools import combinations

# Unfortunately mocks cannot be used here as Numba-compiled code does not
# accept mocks of Numba-compiled classess... (So there are not unit tests
# for the scoring code, only end to end tests.)

class TestScoringEndToEnd(unittest.TestCase):
    """
        End to end tests for the software.
        Ensures that the local scores produced are correct to 
        within 7 decial places of the C version for some real data sets.
        Also ensures that the scores are correctly saved in a format that can
        be understood by GOBNILP.
        
        As these are end to end tests nothing is "mocked out"
    """
    
    #data_sets= glob("test_data_and_scores/*.dat")
    data_sets = [
        "test_data_and_scores/alarm_100.dat",
        "test_data_and_scores/water_1000.dat",
        "test_data_and_scores/asia_10000.dat"
    ]
    
    def read_correct_local_scores(self,filename):
        """
            Read local scores for families (variables+parent set)
            from a file.
            Assumes "Jaakkola" format. Not much error checking.
            (Slightly modified version of the code from simple_gobnilp.py)
        """
        with open(filename,"r") as fobj:
            family_scores = {}
            n = int(fobj.readline())
           
            fields = fobj.readline().rstrip().split()
            def init(fields):
                # no check that there are only two fields
                return fields[0], int(fields[1]), 0, {} 
            current_variable, nscores, i, this_dkt = init(fields)
            for line in fobj:
                fields = line.rstrip().split()
                if i < nscores:
                    # don't bother checking that fields[1] correctly specifies
                    # the number of parents
                    this_dkt[frozenset(fields[2:])] = float(fields[0])
                    i += 1
                else:
                    family_scores[current_variable] = this_dkt
                    current_variable, nscores, i, this_dkt = init(fields)
            family_scores[current_variable] = this_dkt
            
            ordered_parentsets = {}
            for child, scored_parentsets in list(family_scores.items()):
                ordered_parentsets[child] = sorted(scored_parentsets,key=lambda x: scored_parentsets[x],reverse=True)
        return family_scores
        #self._bn_variables = sorted(family_scores)
        #self._n = len(self._bn_variables)
        #self._ordered_parentsets = ordered_parentsets

    def compute_scores(self,filename, palim, use_adtree):
        data = BDeuScoresGenerator(filename, use_adtree, max_palim_size=palim)
        return data.all_bdeu_scores(palim=palim)
    
    def scores_correct(self, computed_scores, correct_scores):
        children = sorted(correct_scores.keys())
        with self.subTest(test="Same children"):
            self.assertEqual(sorted(computed_scores.keys()),children)
        for child in children:
            with self.subTest(child=child):
                candidate_parent_sets = correct_scores[child].keys()
                
                with self.subTest(candidate_parent_sets = candidate_parent_sets):
                    self.assertEqual(
                        frozenset(computed_scores[child].keys()),
                        frozenset(candidate_parent_sets)
                    )
                for parents in candidate_parent_sets:
                    with self.subTest(parents=parents):
                        self.assertAlmostEqual(
                            correct_scores[child][parents],
                            computed_scores[child][parents]
                        )
        
         
    def test_correct_scores_integration_test(self):
        """
            Ensures that that given a data file as input the correct scores are produced.
            Also ensures that they can are correctly saved to a file so that they can be reloaded by GOBNILP.
            The ADTree class is not mocked out, this is a full end to end test.
            
            Note the use of subTest so that it won't fail if just one subtest fails
        """
        for use_adtree in [True, False]:
            for palim in range(4): # Have precomputed scores up to palim=3
                with self.subTest(palim = palim):
                    for data_set in self.data_sets:
                        with self.subTest(data_set = data_set):
                            
                            correct_scores = self.read_correct_local_scores(data_set+"."+str(palim)+".jkl")
                            computed_scores = self.compute_scores(data_set, palim, use_adtree)
                            
                            self.scores_correct(computed_scores, correct_scores)
                            with self.subTest(score_file_saved = data_set):
                                saved_filename = tempfile.gettempdir() + os.sep + data_set.split("/")[1] + "test_scores_file.jkl"
                                save_local_scores(computed_scores, saved_filename)
                                read_from_file = self.read_correct_local_scores(saved_filename)
                                self.scores_correct(read_from_file, computed_scores)
                            
            

if __name__ == '__main__':
    unittest.main()
