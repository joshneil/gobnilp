import unittest

from contab_simple_tree import ContabGenerator
import numpy as np
from itertools import product

""" 
"""


class TestContabGeneratorCounts(unittest.TestCase):
    """Test the counts produced by the contingency tables produced
       by my AD tree implementation for every possible combination
       of variable values for several different data sets"""
    
    data_sets_and_arities = [
        (
            "Data set 1",
            np.array([
                [0,1,1],
                [2,2,2],
                [4,4,4],
                [4,4,4]
            ], dtype=np.uint32),
            np.array(
                [5,6,7], 
                dtype=np.uint32
            )
        ),
        (
            "Data set 2",
            np.array([
                [0,1,1],
                [0,1,2],
                [2,1,2],
                [2,2,2],
                [4,4,4],
                [4,4,4]
            ], dtype=np.uint32),
            np.array(
                [5,6,7], 
                dtype=np.uint32
            )
        ),
        (
            "Data set 3",
            np.array([
                [0,1,1,0,0,0],
                [0,1,2,1,1,2],
                [6,1,2,1,4,4],
                [2,2,2,0,2,1],
                [3,4,4,1,2,1]
            ], dtype=np.uint32),
            np.array(
                [7,6,5,2,5,5], 
                dtype=np.uint32
            )
        ),
        (
            "Data set 4",
            np.array([
                [0,1,1,2],
                [2,2,2,1],
                [4,4,4,0],
                [4,4,4,1],
                [0,1,1,2],
                [0,1,2,1],
                [4,3,4,0],
                [2,4,1,1],
                [0,2,1,2],
                [2,2,2,1],
                [4,1,4,0],
                [4,0,4,1]
            ], dtype=np.uint32),
            np.array(
                [5,6,7,3], 
                dtype=np.uint32
            )
        ),
        (
            "All 0s - 3 rows - 3 variables - arity 1",
            np.array([
                [0,0,0],
                [0,0,0],
                [0,0,0]
            ], dtype=np.uint32),
            np.array(
                [1,1,1], 
                dtype=np.uint32
            )
        ),
        (
            "All 0s - 3 rows - 3 variables - arity 2",
            np.array([
                [0,0,0],
                [0,0,0],
                [0,0,0]
            ], dtype=np.uint32),
            np.array(
                [2,2,2], 
                dtype=np.uint32
            )
        ),
        (
            "All 0s - 3 rows - 3 variables - different arities",
            np.array([
                [0,0,0],
                [0,0,0],
                [0,0,0]
            ], dtype=np.uint32),
            np.array(
                [1,9,10], 
                dtype=np.uint32
            )
        ),
        (
            "All 0s - 5 rows - 3 variables - different arities",
            np.array([
                [0,0,0],
                [0,0,0],
                [0,0,0],
                [0,0,0],
                [0,0,0]
            ], dtype=np.uint32),
            np.array(
                [3,9,10], 
                dtype=np.uint32
            )
        ),
        (
            "All 2s - 3 rows - 3 variables - arity 3",
            np.array([
                [2,2,2],
                [2,2,2],
                [2,2,2]
            ], dtype=np.uint32),
            np.array(
                [3,3,3], 
                dtype=np.uint32
            )
        ),
        (
            "All 2s - 4 rows - 5 variables - arity 4",
            np.array([
                [2,2,2,2,2],
                [2,2,2,2,2],
                [2,2,2,2,2],
                [2,2,2,2,2]
            ], dtype=np.uint32),
            np.array(
                [4,4,4], 
                dtype=np.uint32
            )
        ), 
        (
            "three rows 3 variables",
            np.array([
                [2,2,2],
                [2,2,2],
                [2,2,2],
            ], dtype=np.uint32),
            np.array(
                [3,3,3], 
                dtype=np.uint32
            )
        ),
    ]
        

    def naive_count(self, variables, values, data):
        count = 0
        for row in data:
            inc = True
            for i in range(len(variables)):
                var = variables[i]
                val = values[i]
                if row[var] != val:
                    inc = False
                    break
            if inc:
                count +=1
        return count
    
    def extract_count_from_contab(self, contab, variable_values):
        for row in contab:
            if row[:-1] == variable_values:
                return row[-1]
        
        
    def sublists(self,s):
        """Took this from here: https://www.reddit.com/r/learnpython/comments/2uhczk/all_possible_slices_of_a_list/"""
        length = len(s)
        for size in range(1, length + 1):
            for start in range(0, (length - size) + 1):
                yield s[start:start+size]
                
    
    def test_contabs(self):
        """Note the use of subTest so that it won't fail if just one subtest fails"""
        
           
        for data_set_name, data, arities in self.data_sets_and_arities:
            num_vars = data.shape[1]

            for max_contab_size in range(num_vars+1):
                with self.subTest(max_contab_size=max_contab_size):
                    with self.subTest(data_set=data_set_name):
                        
                        contab_generator = ContabGenerator(data, arities)
                        all_variables = [x for x in range(arities.size)]
                        
                        for vars in self.sublists(all_variables):
                            
                            with self.subTest(variables = vars):
                                # Makes the tests easier if its a list...
                                np_vars_array = np.array(vars,dtype=np.uint32)
                                
                                contab = contab_generator.make_contab_full(np_vars_array)
                                #print("\n\n", vars)
                                #print(contab)
                              
                                contab = contab.tolist()
                                combos = product(*[range(arities[x]) for x in vars])
                                
                                for combo in combos:
                                    
                                    with self.subTest(count_for = combo):
                                        
                                        naive_count = self.naive_count(vars,combo, data)
                                        contab_count = self.extract_count_from_contab(contab, list(combo))
                                        #print(combo,naive_count,contab_count)
                                        self.assertEqual(naive_count, contab_count)
        #print(ADTree.makeContabInternal.parallel_diagnostics(level=4))
            

if __name__ == '__main__':
    unittest.main()